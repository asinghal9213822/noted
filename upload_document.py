#!/usr/bin/env python
# coding: utf-8

# In[13]:


import openai
import pickle
from langchain.document_loaders import PyPDFLoader
openai.api_key = ''


# In[8]:


def save_user(file_nm, file):
    with open(file_nm, 'wb') as f:
        pickle.dump(file, f)


# In[11]:


# save a separate file for the new user
def upload_doc(doc_name):
    new_user_doc = {}    
    loader = PyPDFLoader(doc_name)
    pages = loader.load_and_split()
    new_user_doc[doc_name] = pages
    save_user('new_user_doc.pkl', new_user_doc)

